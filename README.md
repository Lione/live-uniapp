# live-uniapp

#### 介绍
基于uni-app开发的推流直播APP案例原型（目前只在安卓环境做测试，不支持小程序）

#### 软件架构
UI组件: uView

页面组件：subNvue 和 nvue


#### 安装教程

npm install

#### 使用说明

[(一)用uni-app开发直播推流APP——环境安装与推流案例](https://www.jianshu.com/p/df2e81e414ae)

[(二)用uni-app开发直播推流APP——推流界面优化和无线真机调试](https://www.jianshu.com/p/60ed33f75870)

[(三)用uni-app开发直播推流APP——功能逐步细化完善](https://www.jianshu.com/p/f11965521baf)
